"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
/**
 * Custom resolver function for bindings
 * @param ctx The current context
 * @param injection Metadata about the injection
 * @param session Current resolution session
 */
const resolve = (ctx, injection, session) => {
    console.log('Context: %s Binding: %s', ctx.name, session.currentBinding.key);
    const targetName = context_1.ResolutionSession.describeInjection(injection).targetName;
    console.log('Injection: %s', targetName);
    return injection.member === 'prefix' ? new Date().toISOString() : 'John';
};
/**
 * A class with dependency injection
 */
let Greeter = class Greeter {
    constructor(name) {
        this.name = name;
        this.prefix = '';
    }
    hello() {
        return `[${this.prefix}] Hello, ${this.name}`;
    }
};
tslib_1.__decorate([
    (0, context_1.inject)('', {}, resolve),
    tslib_1.__metadata("design:type", Object)
], Greeter.prototype, "prefix", void 0);
Greeter = tslib_1.__decorate([
    tslib_1.__param(0, (0, context_1.inject)('', {}, resolve)),
    tslib_1.__metadata("design:paramtypes", [String])
], Greeter);
const GREETER = context_1.BindingKey.create('greeter');
async function main() {
    const ctx = new context_1.Context('invocation-context');
    ctx.bind(GREETER).toClass(Greeter);
    const greeter = await ctx.get(GREETER);
    console.log(greeter.hello());
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=custom-inject-resolve.js.map