"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const testlab_1 = require("@loopback/testlab");
const fs_1 = require("fs");
const path_1 = tslib_1.__importDefault(require("path"));
const util_1 = require("util");
const __1 = require("../..");
describe('context examples', () => {
    const logs = [];
    const errors = [];
    let originalConsoleLog = console.log;
    let originalConsoleError = console.error;
    before(disableConsoleOutput);
    it('runs all examples', async function () {
        // For some reason, travis CI on mac reports timeout for some builds
        // Error: Timeout of 2000ms exceeded.
        this.timeout(5000);
        const expectedLogs = loadExpectedLogs();
        await (0, __1.main)();
        (0, testlab_1.expect)(errors).to.eql([]);
        (0, testlab_1.expect)(replaceDates(logs)).to.eql(replaceDates(expectedLogs));
    });
    after(restoreConsoleOutput);
    /**
     * Load the expected logs from `fixtures/examples-output.txt`.
     *
     * Run `node . > fixtures/examples-output.txt` to update the logs if needed.
     */
    function loadExpectedLogs() {
        const output = (0, fs_1.readFileSync)(path_1.default.join(__dirname, '../../../fixtures/examples-output.txt'), 'utf-8');
        const items = output.split('\n');
        // When we run `node . > fixtures/examples-output.txt`, a new line is added
        // at the end of the file.
        items.pop();
        return items;
    }
    function disableConsoleOutput() {
        originalConsoleLog = console.log;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        console.log = (fmt, ...params) => {
            logs.push((0, util_1.format)(fmt, ...params));
        };
        originalConsoleError = console.error;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        console.error = (fmt, ...params) => {
            errors.push((0, util_1.format)(fmt, ...params));
        };
    }
    function restoreConsoleOutput() {
        console.log = originalConsoleLog;
        console.error = originalConsoleError;
    }
    function replaceDates(items) {
        return items.map(str => str.replace(/\[\d+[\w\d\-\.\:]+\]/g, '[DATE]'));
    }
});
//# sourceMappingURL=examples.acceptance.js.map