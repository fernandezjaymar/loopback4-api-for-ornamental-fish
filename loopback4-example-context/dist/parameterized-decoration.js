"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
/**
 * A function to create a new class with parameterized decorations
 * @param bindingKeyForName Binding key for current user
 * @param tags Additional binding tags
 */
function createClassWithDecoration(bindingKeyForName, ...tags) {
    let GreeterTemplate = class GreeterTemplate {
        constructor(userName) {
            this.userName = userName;
        }
        hello() {
            return `Hello, ${this.userName}`;
        }
    };
    GreeterTemplate = tslib_1.__decorate([
        (0, context_1.injectable)({ tags }),
        tslib_1.__param(0, (0, context_1.inject)(bindingKeyForName)),
        tslib_1.__metadata("design:paramtypes", [String])
    ], GreeterTemplate);
    return GreeterTemplate;
}
async function main() {
    const ctx = new context_1.Context();
    ctx.bind('name1').to('John');
    ctx.bind('name2').to('Jane');
    const class1 = createClassWithDecoration('name1', { tags: { prefix: '1' } });
    const binding1 = (0, context_1.createBindingFromClass)(class1, { key: 'greeter1' });
    ctx.add(binding1);
    console.log('1:', binding1.tagMap);
    const class2 = createClassWithDecoration('name2', { tags: { prefix: '2' } });
    const binding2 = (0, context_1.createBindingFromClass)(class2, { key: 'greeter2' });
    ctx.add(binding2);
    console.log('2:', binding2.tagMap);
    const greeting1 = await ctx.get('greeter1');
    console.log('1: %s', greeting1.hello());
    const greeting2 = await ctx.get('greeter2');
    console.log('2: %s', greeting2.hello());
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=parameterized-decoration.js.map