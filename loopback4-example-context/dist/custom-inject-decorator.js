"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
const CURRENT_USER = context_1.BindingKey.create('currentUser');
/**
 * Create a decorator to inject the current user name
 */
function whoAmI() {
    return (0, context_1.inject)(CURRENT_USER);
}
/**
 * A class with dependency injection
 */
let Greeter = class Greeter {
    constructor(userName) {
        this.userName = userName;
    }
    hello() {
        return `Hello, ${this.userName}`;
    }
};
Greeter = tslib_1.__decorate([
    tslib_1.__param(0, whoAmI()),
    tslib_1.__metadata("design:paramtypes", [String])
], Greeter);
async function main() {
    const ctx = new context_1.Context('invocation-context');
    ctx.bind(CURRENT_USER).to('John');
    ctx.bind('greeter').toClass(Greeter);
    const greeter = await ctx.get('greeter');
    console.log(greeter.hello());
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=custom-inject-decorator.js.map