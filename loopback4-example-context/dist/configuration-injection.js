"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
/**
 * A greeter service
 */
let Greeter = class Greeter {
    constructor(settings = {}) {
        this.settings = settings;
    }
    greet(name) {
        const prefix = this.settings.prefix ? `${this.settings.prefix}` : '';
        const date = this.settings.includeDate
            ? `[${new Date().toISOString()}]`
            : '';
        return `${date} ${prefix}: Hello, ${name}`;
    }
};
Greeter = tslib_1.__decorate([
    tslib_1.__param(0, (0, context_1.config)()),
    tslib_1.__metadata("design:paramtypes", [Object])
], Greeter);
async function main() {
    const ctx = new context_1.Context();
    // Configure `greeter` with `{prefix: '>>>', includeDate: true}`
    ctx
        .configure('greeter')
        .to({ prefix: '>>>', includeDate: true });
    ctx.bind('greeter').toClass(Greeter);
    const greeter = await ctx.get('greeter');
    console.log(greeter.greet('Ray'));
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=configuration-injection.js.map