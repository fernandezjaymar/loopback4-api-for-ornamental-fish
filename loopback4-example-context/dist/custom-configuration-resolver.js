"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
/**
 * A custom configuration resolver that looks up env variables first
 */
let EnvConfigResolver = class EnvConfigResolver extends context_1.DefaultConfigurationResolver {
    constructor(context) {
        super(context);
        this.context = context;
    }
    /**
     * Try to find a matching env variable (case insensitive)
     * @param key The binding key
     */
    getFromEnvVars(key) {
        let val;
        let found;
        for (const k in process.env) {
            if (k.toUpperCase() === key.toUpperCase()) {
                val = process.env[k];
                found = k;
                break;
            }
        }
        if (val == null)
            return val;
        console.log('Loading configuration for binding "%s" from env variable "%s"', key, found);
        try {
            return JSON.parse(val);
        }
        catch (err) {
            return val;
        }
    }
    getConfigAsValueOrPromise(key, configPath, resolutionOptions) {
        const val = this.getFromEnvVars(key.toString());
        if (val != null)
            return val;
        return super.getConfigAsValueOrPromise(key, configPath, resolutionOptions);
    }
};
EnvConfigResolver = tslib_1.__decorate([
    tslib_1.__param(0, context_1.inject.context()),
    tslib_1.__metadata("design:paramtypes", [context_1.Context])
], EnvConfigResolver);
async function main() {
    const ctx = new context_1.Context();
    ctx.bind(context_1.ContextBindings.CONFIGURATION_RESOLVER).toClass(EnvConfigResolver);
    // Configure `foo` with `{bar: 'abc'}`
    // To override it with env var, use `foo='{"bar":"abc"}'`.
    ctx.configure('foo').to({ bar: 'abc' });
    const fooConfig = await ctx.getConfig('foo');
    console.log(fooConfig);
    ctx.configure('bar').to('xyz');
    const barConfig = ctx.getConfigSync('bar');
    console.log(barConfig);
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=custom-configuration-resolver.js.map