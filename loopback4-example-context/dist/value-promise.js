"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const context_1 = require("@loopback/context");
class ChineseGreeter {
    constructor() {
        this.language = 'zh';
    }
    greet(name) {
        return `[value] 你好，${name}！`;
    }
}
class EnglishGreeter {
    constructor() {
        this.language = 'en';
    }
    greet(name) {
        return `[value] Hello, ${name}!`;
    }
}
class AsyncChineseGreeter {
    constructor() {
        this.language = 'zh';
    }
    /**
     * Async was of greeting
     * @param name - Name
     */
    greet(name) {
        return new Promise(resolve => setImmediate(() => {
            resolve(`[promise] 你好，${name}！`);
        }));
    }
}
async function main() {
    const ctx = new context_1.Context('app');
    // Add EnglishGreeter for now
    ctx.bind('greeters.EnglishGreeter').toClass(EnglishGreeter).tag('greeter');
    // Add ChineseGreeter
    ctx.bind('greeters.ChineseGreeter').toClass(ChineseGreeter).tag('greeter');
    // Find all greeters
    const greetersView = ctx.createView((0, context_1.filterByTag)('greeter'));
    // Greet from all greeters
    await greetFromAll(greetersView);
    // Replace ChineseGreeter with AsyncChineseGreeter
    ctx
        .bind('greeters.ChineseGreeter')
        .toClass(AsyncChineseGreeter)
        .tag('greeter');
    // Greet from all greeters again
    await greetFromAll(greetersView);
}
exports.main = main;
/**
 * Invoke all greeters to print out greetings in all supported langauges
 * @param greetersView - A context view representing all greeters
 */
async function greetFromAll(greetersView) {
    // Get all greeter instances
    const greeters = await greetersView.values();
    // Collect greetings as an array from all greeters
    const greetings = (0, context_1.resolveList)(greeters, greeter => {
        return greeter.greet('John');
    });
    // Check if the result is a Promise (async) or value (sync)
    if ((0, context_1.isPromiseLike)(greetings)) {
        console.log('async:', await greetings);
    }
    else {
        console.log('sync:', greetings);
    }
    // Collect greetings as a map keyed by language from al greeters
    const greeterMap = {};
    greeters.filter(greeter => (greeterMap[greeter.language] = greeter));
    const greetingsByLanguage = (0, context_1.resolveMap)(greeterMap, greeter => greeter.greet('Jane'));
    // Print out all map entries
    await (0, context_1.transformValueOrPromise)(greetingsByLanguage, console.log);
}
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=value-promise.js.map