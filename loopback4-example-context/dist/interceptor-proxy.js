"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
const REQUEST_CONTEXT = context_1.BindingKey.create('request.context');
const REQUEST_ID = context_1.BindingKey.create('tracing.requestId');
const REQUEST_ID_GENERATOR = context_1.BindingKey.create('tracing.requestIdGenerator');
const TRACING_INTERCEPTOR = context_1.BindingKey.create('tracing.interceptor');
const CONVERTER = context_1.BindingKey.create('converter');
const GREETER = context_1.BindingKey.create('greeter');
let TracingInterceptor = class TracingInterceptor {
    constructor(generator) {
        this.generator = generator;
    }
    value() {
        return this.intercept.bind(this);
    }
    async intercept(invocationCtx, next) {
        const reqCtx = invocationCtx.getSync(REQUEST_CONTEXT);
        let reqId = await reqCtx.get(REQUEST_ID, { optional: true });
        if (!reqId) {
            reqId = this.generator(reqCtx);
            // We need to set request id to `reqCtx` as `invocationCtx` is transient
            reqCtx.bind(REQUEST_ID).to(reqId);
            console.log('Adding request id at %s: %s', invocationCtx.targetName, reqId);
        }
        else {
            console.log('Request id found at %s: %s', invocationCtx.targetName, reqId);
        }
        return next();
    }
};
TracingInterceptor = tslib_1.__decorate([
    (0, context_1.injectable)((0, context_1.asGlobalInterceptor)('tracing')),
    tslib_1.__param(0, (0, context_1.inject)(REQUEST_ID_GENERATOR)),
    tslib_1.__metadata("design:paramtypes", [Function])
], TracingInterceptor);
class Converter {
    toUpperCase(name) {
        return name.toUpperCase();
    }
}
class Greeter {
    async greet(name) {
        const msg = await this.converter.toUpperCase(name);
        return `Hello, ${msg}`;
    }
}
tslib_1.__decorate([
    (0, context_1.inject)(CONVERTER, { asProxyWithInterceptors: true }),
    tslib_1.__metadata("design:type", Object)
], Greeter.prototype, "converter", void 0);
async function main() {
    const ctx = new context_1.Context('request');
    // Bind request context
    ctx.bind(REQUEST_CONTEXT).to(ctx);
    const binding = (0, context_1.createBindingFromClass)(TracingInterceptor, {
        key: TRACING_INTERCEPTOR,
    });
    ctx.add(binding);
    let count = 1;
    const reqUuidGenerator = context => `[${context.name}] ${count++}`;
    ctx.bind(REQUEST_ID_GENERATOR).to(reqUuidGenerator);
    ctx.bind(GREETER).toClass(Greeter);
    ctx.bind(CONVERTER).toClass(Converter).tag(context_1.BindingScope.SINGLETON);
    const greeter = await ctx.get(GREETER, { asProxyWithInterceptors: true });
    console.log(await greeter.greet('John'));
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=interceptor-proxy.js.map