"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const context_1 = require("@loopback/context");
const util_1 = require("util");
const setImmediateAsync = (0, util_1.promisify)(setImmediate);
class ChineseGreeter {
    constructor() {
        this.language = 'zh';
    }
    greet(name) {
        return `你好，${name}！`;
    }
}
class EnglishGreeter {
    constructor() {
        this.language = 'en';
    }
    greet(name) {
        return `Hello, ${name}!`;
    }
}
/**
 * Create a subclass of context to wait until all observers notified
 */
class RequestContext extends context_1.Context {
    /**
     * Wait until the context event queue is empty or an error is thrown
     */
    waitUntilObserversNotified() {
        return this.subscriptionManager.waitUntilPendingNotificationsDone(100);
    }
}
async function main() {
    const appCtx = new context_1.Context('app');
    const requestCtx = new RequestContext(appCtx, 'request');
    // Observer events from `appCtx`
    appCtx.subscribe({
        filter: (0, context_1.filterByTag)('greeter'),
        observe: (eventType, binding) => {
            console.log('[observer] %s %s', eventType, binding.key);
        },
    });
    // Create a context view on `requestCtx`
    const greetersView = requestCtx.createView((0, context_1.filterByKey)(/^greeters\./));
    greetersView.on('refresh', () => {
        console.log('[view.refresh] %j', greetersView.bindings.map(b => b.key));
    });
    // Add EnglishGreeter to `appCtx`
    console.log('Adding EnglishGreeter');
    appCtx.bind('greeters.EnglishGreeter').toClass(EnglishGreeter).tag('greeter');
    // Add ChineseGreeter to `appCtx`
    await setImmediateAsync();
    console.log('Adding ChineseGreeter');
    appCtx.bind('greeters.ChineseGreeter').toClass(ChineseGreeter).tag('greeter');
    // Remove ChineseGreeter from `appCtx`
    await setImmediateAsync();
    console.log('Removing ChineseGreeter');
    appCtx.unbind('greeters.ChineseGreeter');
    // Add ChineseGreeter to `requestCtx`
    await setImmediateAsync();
    console.log('Adding ChineseGreeter to request context');
    requestCtx
        .bind('greeters.ChineseGreeter')
        .toClass(ChineseGreeter)
        .tag('greeter');
    await requestCtx.waitUntilObserversNotified();
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=context-observation.js.map