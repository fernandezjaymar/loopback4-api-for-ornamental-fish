"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
/**
 * A strongly-typed binding key for current date
 */
const CURRENT_DATE = context_1.BindingKey.create('currentDate');
/**
 * A strongly-typed binding key for `GreetingService`
 */
const GREETING_SERVICE = context_1.BindingKey.create('services.GreetingService');
/**
 * A factory function to return the current date
 */
const getCurrentDate = () => new Date();
class ChineseGreeter {
    constructor() {
        this.language = 'zh';
    }
    greet(name) {
        return `你好，${name}！`;
    }
}
class EnglishGreeter {
    constructor() {
        this.language = 'en';
    }
    greet(name) {
        return `Hello, ${name}!`;
    }
}
/**
 * A class with dependency injection
 */
let GreetingService = class GreetingService {
    // Constructor based injection
    constructor(greetersView) {
        this.greetersView = greetersView;
    }
    async greet(language, userName) {
        // Get current date
        const date = await this.now();
        // Get current list of greeters
        const greeters = await this.greetersView.values();
        for (const greeter of greeters) {
            if (greeter.language === language) {
                const msg = greeter.greet(userName);
                return `[${date.toISOString()}] (${language}) ${msg}`;
            }
        }
        return `[${date.toISOString()}] Hello, ${userName}!`;
    }
};
tslib_1.__decorate([
    context_1.inject.getter(CURRENT_DATE),
    tslib_1.__metadata("design:type", Function)
], GreetingService.prototype, "now", void 0);
tslib_1.__decorate([
    tslib_1.__param(0, (0, context_1.inject)('currentLanguage')),
    tslib_1.__param(1, (0, context_1.inject)('currentUser')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], GreetingService.prototype, "greet", null);
GreetingService = tslib_1.__decorate([
    tslib_1.__param(0, context_1.inject.view((0, context_1.filterByTag)('greeter'))),
    tslib_1.__metadata("design:paramtypes", [context_1.ContextView])
], GreetingService);
async function main() {
    const ctx = new context_1.Context('request');
    // Bind greeting service
    ctx.bind(GREETING_SERVICE).toClass(GreetingService);
    // Set the current date to a factory function
    ctx.bind(CURRENT_DATE).toDynamicValue(getCurrentDate);
    // Set the current user to `John` (a constant value)
    ctx.bind('currentUser').to('John');
    // Set the current language to `zh`
    ctx.bind('currentLanguage').to('zh');
    // Add EnglishGreeter for now
    ctx.bind('greeters.EnglishGreeter').toClass(EnglishGreeter).tag('greeter');
    // Get an instance of the greeting service
    const greetingService = await ctx.get(GREETING_SERVICE);
    // Invoke `greet` as a method
    let greeting = await greetingService.greet('en', 'Jane');
    console.log(greeting);
    // Use `invokeMethod` to apply method injection
    greeting = await (0, context_1.invokeMethod)(greetingService, 'greet', ctx);
    console.log(greeting);
    // Now add ChineseGreeter
    ctx.bind('greeters.ChineseGreeter').toClass(ChineseGreeter).tag('greeter');
    greeting = await (0, context_1.invokeMethod)(greetingService, 'greet', ctx);
    console.log(greeting);
    // Change the current language to `en`
    ctx.bind('currentLanguage').to('en');
    // Change the current user to `Jane`
    ctx.bind('currentUser').to('Jane');
    greeting = await (0, context_1.invokeMethod)(greetingService, 'greet', ctx);
    console.log(greeting);
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=dependency-injection.js.map