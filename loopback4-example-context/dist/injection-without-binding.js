"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
/**
 * Strongly-typed binding key for the current user
 */
const CURRENT_USER = context_1.BindingKey.create('currentUser');
/**
 * A class with dependency injection
 */
let Greeter = class Greeter {
    constructor(userName) {
        this.userName = userName;
    }
    hello() {
        return `Hello, ${this.userName}`;
    }
};
Greeter = tslib_1.__decorate([
    tslib_1.__param(0, (0, context_1.inject)(CURRENT_USER)),
    tslib_1.__metadata("design:paramtypes", [String])
], Greeter);
/**
 * Create an instance for `Greeter` with the given context and run `hello()`
 * @param ctx Context object
 */
async function sayHello(ctx) {
    const greeter = await (0, context_1.instantiateClass)(Greeter, ctx);
    console.log(greeter.hello());
}
async function main() {
    const ctx = new context_1.Context('invocation-context');
    ctx.bind(CURRENT_USER).to('John');
    await sayHello(ctx);
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=injection-without-binding.js.map