"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
var RequestIdProvider_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
/**
 * A strongly-typed binding key for current date
 */
const CURRENT_DATE = context_1.BindingKey.create('currentDate');
/**
 * A strongly-typed binding key for `Greeter`
 */
const GREETER = context_1.BindingKey.create('greeter');
/**
 * A factory function to return the current date
 */
const getCurrentDate = () => new Date();
/**
 * A class with dependency injection
 */
let Greeter = class Greeter {
    // Constructor based injection
    constructor(userName) {
        this.userName = userName;
    }
    hello() {
        return `[${this.created.toISOString()}] (${this.requestId}) Hello, ${this.userName}`;
    }
};
tslib_1.__decorate([
    (0, context_1.inject)(CURRENT_DATE),
    tslib_1.__metadata("design:type", Date)
], Greeter.prototype, "created", void 0);
tslib_1.__decorate([
    (0, context_1.inject)('requestId'),
    tslib_1.__metadata("design:type", String)
], Greeter.prototype, "requestId", void 0);
Greeter = tslib_1.__decorate([
    tslib_1.__param(0, (0, context_1.inject)('currentUser')),
    tslib_1.__metadata("design:paramtypes", [String])
], Greeter);
/**
 * A provider is similar as the factory function but it requires dependency
 * injection. As a result, it's declared as a class with `@inject.*` decorations
 * applied.
 */
let RequestIdProvider = RequestIdProvider_1 = class RequestIdProvider {
    // Injection of `url`
    constructor(url) {
        this.url = url;
    }
    // This method returns the resolved value for the binding
    value() {
        let id = RequestIdProvider_1.ids.get(this.url);
        if (id == null) {
            id = 1;
        }
        else {
            id++;
        }
        RequestIdProvider_1.ids.set(this.url, id);
        return `${this.url}#${id}`;
    }
};
RequestIdProvider.ids = new Map();
RequestIdProvider = RequestIdProvider_1 = tslib_1.__decorate([
    tslib_1.__param(0, (0, context_1.inject)('url')),
    tslib_1.__metadata("design:paramtypes", [String])
], RequestIdProvider);
async function main() {
    const ctx = new context_1.Context('request');
    // Set the current user to `John` (a constant value)
    ctx.bind('currentUser').to('John');
    // Set current url
    ctx.bind('url').to('/greet');
    // Set the current date to a factory function that creates the value
    ctx.bind(CURRENT_DATE).toDynamicValue(getCurrentDate);
    // Bind `hello` to a class from which the value is instantiated
    ctx.bind(GREETER).toClass(Greeter);
    // Set `requestId` to a provider class which has a `value()` method to
    // create the value. It's a specializd factory declared as a class to allow
    // dependency injection
    ctx.bind('requestId').toProvider(RequestIdProvider);
    // Bind `hello` as an alias to `GREETER`
    ctx.bind('hello').toAlias(GREETER);
    let greeter = await ctx.get(GREETER);
    console.log(greeter.hello());
    greeter = await ctx.get('hello');
    console.log(greeter.hello());
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=binding-types.js.map