"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019,2020. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
const asGreeter = binding => {
    binding.tag('greeter');
};
const greeterFilter = binding => binding.tagMap['greeter'] != null;
class ChineseGreeter {
    constructor() {
        this.language = 'zh';
    }
    greet(name) {
        return `你好，${name}！`;
    }
}
let EnglishGreeter = class EnglishGreeter {
    constructor() {
        this.language = 'en';
    }
    greet(name) {
        return `Hello, ${name}!`;
    }
};
EnglishGreeter = tslib_1.__decorate([
    (0, context_1.injectable)(asGreeter)
], EnglishGreeter);
async function main() {
    const ctx = new context_1.Context('request');
    // Add EnglishGreeter for now
    ctx.add((0, context_1.createBindingFromClass)(EnglishGreeter, { namespace: 'greeters' }));
    // Add ChineseGreeter
    ctx.bind('greeters.ChineseGreeter').toClass(ChineseGreeter).tag('greeter');
    const enlishGreeterBinding = ctx.getBinding('greeters.EnglishGreeter');
    console.log(enlishGreeterBinding.key);
    let possibleEnglishGreeters = ctx.find('*.EnglishGreeter');
    console.log(possibleEnglishGreeters.map(b => b.key));
    possibleEnglishGreeters = ctx.find(/\w+\.EnglishGreeter$/);
    console.log(possibleEnglishGreeters.map(b => b.key));
    let greeterBindings = ctx.findByTag('greeter');
    console.log(greeterBindings.map(b => b.key));
    greeterBindings = ctx.find((0, context_1.filterByTag)('greeter'));
    console.log(greeterBindings.map(b => b.key));
    greeterBindings = ctx.find(greeterFilter);
    console.log(greeterBindings.map(b => b.key));
    const view = ctx.createView(greeterFilter, (b1, b2) => b1.key.localeCompare(b2.key));
    console.log(view.bindings.map(b => b.key));
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=find-bindings.js.map