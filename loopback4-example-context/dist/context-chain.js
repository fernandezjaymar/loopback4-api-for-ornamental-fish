"use strict";
// Copyright IBM Corp. and LoopBack contributors 2019. All Rights Reserved.
// Node module: @loopback/example-context
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = void 0;
const tslib_1 = require("tslib");
const context_1 = require("@loopback/context");
let Greeter = class Greeter {
    constructor(prefix) {
        this.prefix = prefix;
    }
    greet(name) {
        return `[${this.prefix}] Hello, ${name}!`;
    }
};
Greeter = tslib_1.__decorate([
    tslib_1.__param(0, (0, context_1.inject)('prefix')),
    tslib_1.__metadata("design:paramtypes", [String])
], Greeter);
async function main() {
    const appCtx = new context_1.Context('app');
    // Create a context per request, with `appCtx` as the parent
    const requestCtx = new context_1.Context(appCtx, 'request');
    const greeterBinding = appCtx
        .bind('services.Greeter')
        .toClass(Greeter)
        .tag('greeter');
    // Set prefix to `app` at app context level
    appCtx.bind('prefix').to(appCtx.name);
    // Get a greeter from request context
    let greeter = await requestCtx.get(greeterBinding.key);
    // Inherit `prefix` from app context
    console.log(greeter.greet('John'));
    // Set `prefix` at request context level
    requestCtx.bind('prefix').to(requestCtx.name);
    greeter = await requestCtx.get(greeterBinding.key);
    // Now the request context prefix is used
    console.log(greeter.greet('John'));
    // Get a greeter from app context
    greeter = await appCtx.get(greeterBinding.key);
    // Now the app context prefix is used
    console.log(greeter.greet('John'));
}
exports.main = main;
if (require.main === module) {
    main().catch(err => {
        console.error(err);
        process.exit(1);
    });
}
//# sourceMappingURL=context-chain.js.map