import {HttpErrors} from '@loopback/rest';
import isemail from 'isemail';
import {User} from '../models';
import {Credentials} from '../repositories/user.repository';

export function validateCredentials(credentials: Credentials) {
  // Validate email
  if (!isemail.validate(credentials.email)) {
    throw new HttpErrors.UnprocessableEntity('Email format is invalid');
  }

  // Validate password length
  if (!credentials.password || credentials.password.length < 8) {
    throw new HttpErrors.UnprocessableEntity('Password number of characters should be greater than 8');
  }
}
export function validatePassword(password: string) {

  //Validate password
  if (password.length < 8) {
    throw new HttpErrors.UnprocessableEntity(
      'Password number of characters should be greater than 8'
    );
  }
}

export function accessAccordingRole(user: User, role: string ) {
  if (!user.roles.includes(role)) {
    throw new HttpErrors.UnprocessableEntity(`user email ${user.email} has no access`);
  }
}
