import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {OrnamentalFishDbDataSource} from '../datasources';
import {Ornamentalfish, OrnamentalfishRelations} from '../models';

export class OrnamentalfishRepository extends DefaultCrudRepository<
  Ornamentalfish,
  typeof Ornamentalfish.prototype.id,
  OrnamentalfishRelations
> {
  constructor(
    @inject('datasources.ornamentalFishDb') dataSource: OrnamentalFishDbDataSource,
  ) {
    super(Ornamentalfish, dataSource);
  }
}
