export type UserRegisterData = {
  username: string;
  email: string;
  password: string;
  name: string;
};
