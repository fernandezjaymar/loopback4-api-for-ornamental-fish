import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {Ornamentalfish} from '../models';
import {OrnamentalfishRepository} from '../repositories';

export class OrnamentalFishController {
  constructor(
    @repository(OrnamentalfishRepository)
    public ornamentalfishRepository : OrnamentalfishRepository,
  ) {}

  @authenticate('jwt')
  @authorize({allowedRoles: ['admin']})
  @post('/ornamentalfish')
  @response(200, {
    description: 'Ornamentalfish model instance',
    content: {'application/json': {schema: getModelSchemaRef(Ornamentalfish)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Ornamentalfish, {
            title: 'NewOrnamentalfish',
            exclude: ['id'],
          }),
        },
      },
    })
    ornamentalfish: Omit<Ornamentalfish, 'id'>,
  ): Promise<Ornamentalfish> {
    return this.ornamentalfishRepository.create(ornamentalfish);
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['admin']})
  @get('/ornamentalfish/count')
  @response(200, {
    description: 'Ornamentalfish model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Ornamentalfish) where?: Where<Ornamentalfish>,
  ): Promise<Count> {
    return this.ornamentalfishRepository.count(where);
  }

  @get('/ornamentalfish')
  @response(200, {
    description: 'Array of Ornamentalfish model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Ornamentalfish, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Ornamentalfish) filter?: Filter<Ornamentalfish>,
  ): Promise<Ornamentalfish[]> {
    return this.ornamentalfishRepository.find(filter);
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['admin']})
  @patch('/ornamentalfish')
  @response(200, {
    description: 'Ornamentalfish PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Ornamentalfish, {partial: true}),
        },
      },
    })
    ornamentalfish: Ornamentalfish,
    @param.where(Ornamentalfish) where?: Where<Ornamentalfish>,
  ): Promise<Count> {
    return this.ornamentalfishRepository.updateAll(ornamentalfish, where);
  }

  @get('/ornamentalfish/{id}')
  @response(200, {
    description: 'Ornamentalfish model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Ornamentalfish, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Ornamentalfish, {exclude: 'where'}) filter?: FilterExcludingWhere<Ornamentalfish>
  ): Promise<Ornamentalfish> {
    return this.ornamentalfishRepository.findById(id, filter);
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['admin']})
  @patch('/ornamentalfish/{id}')
  @response(204, {
    description: 'Ornamentalfish PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Ornamentalfish, {partial: true}),
        },
      },
    })
    ornamentalfish: Ornamentalfish,
  ): Promise<void> {
    await this.ornamentalfishRepository.updateById(id, ornamentalfish);
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['admin']})
  @put('/ornamentalfish/{id}')
  @response(204, {
    description: 'Ornamentalfish PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() ornamentalfish: Ornamentalfish,
  ): Promise<void> {
    await this.ornamentalfishRepository.replaceById(id, ornamentalfish);
  }

  @authenticate('jwt')
  @authorize({allowedRoles: ['admin']})
  @del('/ornamentalfish/{id}')
  @response(204, {
    description: 'Ornamentalfish DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.ornamentalfishRepository.deleteById(id);
  }
}
