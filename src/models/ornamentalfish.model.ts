import {Entity, model, property} from '@loopback/repository';

@model()
export class Ornamentalfish extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  fishName: string;

  @property({
    type: 'string',
  })
  fishScientificName?: string;

  @property({
    type: 'string',
  })
  gender?: string;

  @property({
    type: 'string',
  })
  habitat?: string;

  @property({
    type: 'string',
  })
  strain?: string;

  @property({
    type: 'number',
  })
  price?: number;


  constructor(data?: Partial<Ornamentalfish>) {
    super(data);
  }
}

export interface OrnamentalfishRelations {
  // describe navigational properties here
}

export type OrnamentalfishWithRelations = Ornamentalfish & OrnamentalfishRelations;
